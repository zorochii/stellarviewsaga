extends BaseEvent

func _ready():
	trigger = Ev.Trigger.BUTTON
	list = [
	[Ev.Code.MESSAGE, "Hello world! Testing this interpreter system thing..."],
	[Ev.Code.CHOICES, 0, ["Add experience","Battle!","Teleport!", "Save"], 4],
	[Ev.Code.COMP_VAR, 0, Ev.Comp.EQUAL, 0, "0"],
	[Ev.Code.COMP_VAR, 0, Ev.Comp.EQUAL, 1, "1"],
	[Ev.Code.COMP_VAR, 0, Ev.Comp.EQUAL, 2, "2"],
	[Ev.Code.COMP_VAR, 0, Ev.Comp.EQUAL, 3, "3"],
	[Ev.Code.GOTO, "end"],
	[Ev.Code.LABEL, "0"],
		[Ev.Code.MESSAGE, "Adding 1000EXP to \\n[1]."],
		[Ev.Code.ADD_EXP, true, 0, 1000],
	[Ev.Code.GOTO, "end"],
	[Ev.Code.LABEL, "1"],
		[Ev.Code.MESSAGE, "Calling battle."],
		[Ev.Code.CALL_BATTLE, 0],
	[Ev.Code.GOTO, "end"],
	[Ev.Code.LABEL, "2"],
		[Ev.Code.MESSAGE, "Let's go somewhere else."],
		[Ev.Code.TRANSFER, "SecondScene", 320, 240],
	[Ev.Code.LABEL, "3"],
		[Ev.Code.CALL_SAVE],
	[Ev.Code.GOTO, "end"],
	[Ev.Code.LABEL, "end"],
	]
