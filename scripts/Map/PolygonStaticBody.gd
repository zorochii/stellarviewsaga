extends Polygon2D

export var visibleOnPlay = false

func _ready():
	var collision_shape = CollisionPolygon2D.new()
	collision_shape.polygon = polygon
	var body = StaticBody2D.new()
	body.add_child(collision_shape)
	add_child(body)
	visible = visibleOnPlay
