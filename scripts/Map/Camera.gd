extends Node2D

export var targetPath: NodePath
export var smoothFactor = 0.5

var target = null

func _ready():
	target = get_node(targetPath)
	self.position = target.position

func _process(delta):
	self.position = lerp(self.position, target.position, smoothFactor)
