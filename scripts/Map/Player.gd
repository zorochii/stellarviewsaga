extends KinematicBody2D
class_name Player

export var speed = 256
export var dash_mult = 2

var velocity = Vector2.ZERO
var interpreter = null
var state = null
var nearby_events = []

func _ready():
	interpreter = get_node("/root/Node2D/Game/Interpreter")
	state = get_node("/root/Node2D/Game/State")
	if (state != null):
		var party = state.get_node("Party")
		var member = party.get_actor(true, 0)
		$Sprite.texture = member.data.defaultChar

func _process(delta):
	if (interpreter.busy()):
		return
	if (Input.is_action_just_pressed("ui_accept")):
		var closer = get_closer_event()
		if (closer != null):
			closer.run()

func get_closer_event():
	var retVal = null
	var dst = 0
	for i in nearby_events:
		if (retVal == null):
			retVal = i
			dst = position.distance_to(i.position)
		else:
			var d = position.distance_to(i.position)
			if (dst > d):
				retVal = i
				dst = d
	return retVal

func _physics_process(delta):
	if (interpreter.busy()):
		return
	var direction = get_input_direction()
	if (Input.is_action_pressed("ui_shift")):
		direction *= dash_mult
	move(direction)

func move(direction):
	velocity.x = direction.x * speed
	velocity.y = direction.y * speed
	velocity = move_and_slide(velocity)

func get_input_direction():
	var direction = Vector2.ZERO
	if Input.is_action_pressed("ui_right"):
		direction.x += 1
	if Input.is_action_pressed("ui_left"):
		direction.x -= 1
	if Input.is_action_pressed("ui_up"):
		direction.y -= 1
	if Input.is_action_pressed("ui_down"):
		direction.y += 1
	if direction != Vector2.ZERO:
		direction = direction.normalized()
	return direction

func register_event(ev):
	nearby_events.append(ev)

func remove_event(ev):
	nearby_events.erase(ev)
