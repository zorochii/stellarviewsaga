extends Node
class_name Interpreter

export(State.Scene) var ocassion

onready var message = get_node("/root/Node2D/Game/MessageSystem")
onready var state: State = get_node("/root/Node2D/Game/State")

var list = []
var idx = 0
var running = false
var event = null
var evPath = ""
var wait_count: float = 0.0

func save():
	var dict = {
		"name" : "Interpreter",
		"idx" : idx,
		"running" : running,
		"event" : evPath,
		"wait_count" : wait_count
	}
	return dict

func run(l, ev):
	idx = 0
	list = l
	running = true
	event = ev
	evPath = event.get_path()
	event.set_running(true)

func _process(delta):
	if (wait_count > 0):
		wait_count -= delta
		return
	if (message.is_showing()):
		return
	if (running):
		if (process_command()):
			idx += 1
		if (idx >= list.size()):
			running = false
			if (event != null): event.set_running(false)

func next_command_code():
	var nextIdx = idx + 1
	if (nextIdx >= list.size()):
		return Ev.Code.NONE
	return list[nextIdx][0]

func process_command():
	var command = list[idx]
	match command[0]:
		Ev.Code.MESSAGE: # message
			return process_message(command)
		Ev.Code.CHOICES: # show choices
			return process_choices(command)
		Ev.Code.SET_VAR:
			state.set_var(command[1],command[2])
		Ev.Code.SET_FLAG:
			state.set_flag(command[1],command[2])
		Ev.Code.COMP_VAR: # [c, id, v, label]
			if check_var(command[1], command[2], command[3]):
				look_label(command[4])
		Ev.Code.COMP_FLAG:
			if check_flag(command[1], command[2], command[3]):
				look_label(command[4])
		Ev.Code.GOTO:
			look_label(command[1])
		Ev.Code.ADD_EXP:
			add_exp(command[1], command[2], command[3])
		Ev.Code.CALL_BATTLE:
			call_battle(command[1])
		Ev.Code.TRANSFER:
			execute_transfer(command[1], command[2], command[3])
		Ev.Code.CALL_SAVE:
			call_save()
		Ev.Code.CALL_LOAD:
			call_load()
	return true

func check_var(id, comp, v):
	var value = state.get_var(id)
	match(comp):
		Ev.Comp.GREATEREQ:
			return value >= v
		Ev.Comp.GREATER:
			return value > v
		Ev.Comp.LESS:
			return value < v
		Ev.Comp.LESSEQ:
			return value <= v
		Ev.Comp.NOTEQUAL:
			return value != v
		_:
			return value == v

func check_flag(id, comp, v):
	var value = state.get_flag(id)
	if (comp == Ev.Comp.NOTEQUAL):
		return value != v
	return value == v

func look_label(name):
	for i in range(idx, list.size()):
		var command = list[i]
		if (command[0] == Ev.Code.LABEL):
			if (command[1] == name):
				idx = i
				return

func process_message(command):
	if (message.is_showing()):
		return false
	message.show(command[1])
	if (next_command_code() == Ev.Code.CHOICES):
		idx += 1
		return process_command()
	return true

func process_choices(command):
	message.set_options(command[1], command[2], command[3])
	return true

func add_exp(by_party_slot, i, amount):
	var actor = get_actor(by_party_slot, i)
	actor.change_exp(amount)

func call_battle(battle_id):
	var battle = state.get_node("../Battle")
	battle.start_battle()

func execute_transfer(name, arg1, arg2):
	var map = state.get_node("Map")
	event = null
	if (arg1 == null):
		map.load_scene(name)
		return
	if (arg2 == null):
		map.transfer_to(name, arg1)
		return
	map.transfer(name, arg1, arg2)

func call_save():
	state.save_game(1)
	pass
func call_load():
	state.load_game(1)
	pass

func get_actor(by_party_slot, i):
	return state.get_node("Party").get_actor(by_party_slot, i)

func busy():
	return is_running() || message.is_showing() || state.state != ocassion || state.transition.visible

func is_running():
	return running
