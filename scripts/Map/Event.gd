extends Node
class_name BaseEvent

export var trigger = Ev.Trigger.BUTTON
var running = false
var list = []

var interpreter = null
var inner_interpreter = null

func run():
	if (running):
		return
	interpreter.run(list, self)

func run_parallel():
	if (running):
		return
	if (inner_interpreter == null):
		inner_interpreter = Interpreter.new
		add_child(inner_interpreter)
	inner_interpreter.run(list, self)

func _process(delta):
	check_interpreter()
	if (interpreter == null):
		return
	if (trigger == Ev.Trigger.PARALLEL):
		run_parallel()
		return
	if (interpreter.is_running()):
		return
	if (trigger == Ev.Trigger.AUTO):
		run()

func set_running(v):
	running = v

func check_interpreter():
	if (interpreter == null):
		interpreter = get_node("/root/Node2D/Game/Interpreter")

func _on_body_entered(body):
	if (body is Player):
		if (trigger == Ev.Trigger.COLLISION):
			run()
		elif (trigger == Ev.Trigger.BUTTON):
			body.register_event(self)

func _on_body_exited(body):
	if (body is Player):
		if (trigger == Ev.Trigger.BUTTON):
			body.remove_event(self)
