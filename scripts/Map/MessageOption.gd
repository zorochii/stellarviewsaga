extends NinePatchRect

export var textureNormal: Texture
export var textureSelected: Texture
export var labelPath: NodePath
export var selectedTextColor: Color

onready var label = get_node(labelPath)

var index = 0
var _msgRef = null

func set_option(idx, text, msgRef):
	label.text = text
	index = idx
	_msgRef = msgRef

func set_selected(idx):
	if (index == idx):
		self.texture = textureSelected
		label.modulate = selectedTextColor
	else:
		self.texture = textureNormal
		label.modulate = Color(1, 1, 1)

# Mouse support
var _mouseover = false

func _input(event):
	if event is InputEventMouseButton:
		if _mouseover == true:
			_msgRef.set_index(index)
			print("Selected " + index)

func _on_Area2D_mouse_entered():
	_mouseover = true
	print("Hover over " + index)

func _on_Area2D_mouse_exited():
	_mouseover = false
