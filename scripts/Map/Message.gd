extends Node

export var boxNodePath: NodePath
export var textNodePath: NodePath
export var waitIconPath: NodePath
export var optionParentPath: NodePath
export var optionProtoPath: NodePath
export var statePath: NodePath
export var charsPerSecond = 10
export var speedUpFactor = 10

onready var textNode = get_node(textNodePath)
onready var boxNode = get_node(boxNodePath)
onready var waitNode = get_node(waitIconPath)
onready var optionParent = get_node(optionParentPath)
onready var optionProto = get_node(optionProtoPath)
onready var state = get_node(statePath)

var timer = 0.0
var input_delay_just_pressed = 0.2
var input_delay_pressed = 0.1
var option_buttons = []
var working_cancel = -1
var working_var_id = 0
var option_index = 0
var options_visible = false

func _ready():
	boxNode.visible = false

func _process(delta):
	# Message box processing
	if (boxNode.visible):
		if (textNode.visible_characters < textNode.text.length()):
			var speed = charsPerSecond
			if (Input.is_action_pressed("ui_accept")):
				speed = speedUpFactor
			var secondsPerCharacter = 1.0/speed
			timer += delta
			if (timer > secondsPerCharacter):
				var increment = round(timer / secondsPerCharacter)
				textNode.visible_characters += increment
				timer -= secondsPerCharacter
		else:
			if (option_buttons.size() <= 0):
				waitNode.visible = true
				if (Input.is_action_just_pressed("ui_accept")):
					boxNode.visible = false
	# Options processing
	if (option_buttons.size() > 0):
		if (boxNode.visible && textNode.visible_characters < textNode.text.length()):
			return
		if (!options_visible):
			timer = 0
			show_options()
		else:
			if (timer <= 0):
				# input for options
				if (get_repeat("ui_down")):
					move_index(1)
					return
				if (get_repeat("ui_up")):
					move_index(-1)
					return
			else:
				check_repeat(["ui_up","ui_down"])
				timer -= delta
			if (Input.is_action_just_pressed("ui_accept")):
				boxNode.visible = false
				state.set_var(working_var_id, option_index)
				unset_options()
			if (Input.is_action_just_pressed("ui_cancel")):
				if (working_cancel > -1):
					boxNode.visible = false
					state.set_var(working_var_id, working_cancel)
					unset_options()

func show(text):
	text = process_text(text)
	textNode.text = text
	textNode.visible_characters = 0
	timer = 0.0
	boxNode.visible = true
	waitNode.visible = false

func is_showing():
	return boxNode.visible || options_visible

func process_text(text):
	var regex = RegEx.new()
	# Process parametrized expressions
	regex.compile("\\\\([\\w]+)\\[([0-9]+)\\]")
	for result in regex.search_all(text):
		var rr = result.get_string(0)
		var c = result.get_string(1)
		var v = result.get_string(2)
		var n = process_escape_character(c,v)
		text = text.replace(rr, n)
	# Process no-parameter expressions
	regex.compile("\\\\([\\w]+)")
	for result in regex.search_all(text):
		var rr = result.get_string(0)
		var c = result.get_string(1)
		var n = process_escape_character(c,"0")
		text = text.replace(rr, n)
	return text

func process_escape_character(c,v):
	var n = v.to_int()
	match c:
		"v":
			return state.get_var(n);
		"n":
			var party = state.get_node("Party")
			var a = null
			if (n < 1):
				a = party.get_actor(true, -n)
			else:
				a = party.get_actor(false, n)
			if (a != null):
				return a.displayName
			return ""
		_:
			return ""

# Message options
func set_options(vId, options, on_cancel):
	working_var_id = vId
	working_cancel = on_cancel
	var i = 0
	unset_options()
	for o in options:
		var c = optionProto.duplicate()
		optionParent.add_child(c)
		option_buttons.append(c)
		c.set_option(i, o, self)
		i += 1
	set_index(0)

func show_options():
	for c in option_buttons:
		c.visible = true
	options_visible = true

func unset_options():
	for c in option_buttons:
		c.queue_free()
	option_buttons.clear()
	options_visible = false

# Window option select
func set_index(idx):
	option_index = idx
	for c in option_buttons:
		c.set_selected(idx)

func move_index(dir):
	var idx = (option_index + dir + option_buttons.size()) % option_buttons.size()
	set_index(idx)

func get_repeat(action):
	if (Input.is_action_pressed(action)):
		if (Input.is_action_just_pressed(action)):
			timer = input_delay_just_pressed
		else:
			timer = input_delay_pressed
		return true
	return false

func check_repeat(actions):
	for a in actions:
		if (Input.is_action_just_released(a)):
			timer = 0
