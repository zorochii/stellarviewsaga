extends Node
class_name Map

export var statePath: NodePath
export var first_scene: String = "TestScene"

onready var root = get_node("/root/Node2D")
onready var state = get_node(statePath)
var current_scene = null
var transferring = false
var transfer_reposition = false
var transfer_name = ""
var transfer_pos_x: int = 0
var transfer_pos_y: int = 0

func save():
	var p: Node2D = get_player()
	var dict = {
		"transfer_name" : transfer_name,
		"transfer_pos_x" : p.position.x,
		"transfer_pos_y" : p.position.y
	}
	return dict

func get_camera():
	return current_scene.get_node("Camera2D")

func _ready():
	load_scene(first_scene)

func _process(delta):
	if (transferring == true):
		do_load_scene(transfer_name)
		transferring = false

func transfer_to(name, id):
	# TODO: Cache gate ID to position character over
	load_scene(name)

func transfer(name, x, y):
	transfer_reposition = true
	transfer_pos_x = x
	transfer_pos_y = y
	load_scene(name)

func load_scene(name):
	transfer_name = name
	transferring = true

func do_load_scene(name):
	state.transition.start_transition("Transition")
	if (current_scene != null):
		root.remove_child(current_scene)
		current_scene.call_deferred("free")
		current_scene = null
	var scene_res = load("res://maps/%s.tscn" % name)
	yield(get_tree(), "idle_frame")
	current_scene = scene_res.instance()
	root.add_child(current_scene)
	if (transfer_reposition == true):
		reposition_player(transfer_pos_x, transfer_pos_y)
		transfer_reposition = false

func reposition_player(x,y):
	var p: Node2D = get_player()
	if (p != null):
		p.position.x = x
		p.position.y = y

func get_player() -> Node2D:
	var p = get_tree().get_nodes_in_group("player")
	if (p.size() > 0): return p[0]
	return null
