extends Node
class_name GameParty

export(Array, int) var starting_members = [0]

var members = []
var actors = {}

func _ready():
	for i in starting_members:
		var actor = GameActor.new()
		actor.setup(i)
		actors[i] = actor
		add_child(actor)
		members.append(i)
		pass

func get_actor(by_party_slot, i):
	if (by_party_slot):
		if (members.size() <= i): return null
		return actors[members[i]]
	if (actors.has(i)):
		return actors[i]
	return null

func save():
	pass
