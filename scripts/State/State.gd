extends Node
class_name State

enum Scene {
	MAP,
	MENU,
	BATTLE
}

export(NodePath) var transitionPath

onready var transition = get_node(transitionPath)

var state = Scene.MAP
var variables = []
var flags = []

func set_var(i, v):
	if (i >= variables.size()):
		resize_ary(variables, i, 0)
	variables[i] = v

func set_flag(i, v):
	if (i >= flags.size()):
		resize_ary(flags, i, false)
	flags[i] = v

func get_var(i):
	if (variables.size() <= i):
		return 0
	return variables[i]
	
func get_flag(i):
	if (flags.size() <= i):
		return 0
	return flags[i]

func resize_ary(ary, newSize, default):
	var oldSize = ary.size()
	ary.resize(newSize+1)
	for i in range(oldSize, newSize):
		ary[i] = default

func save_game(index):
	var save_game = File.new()
	save_game.open("user://save%03d.save" % index, File.WRITE)
	var save_nodes = get_tree().get_nodes_in_group("persist")
	for node in save_nodes:
		if !node.has_method("save"):
			print("persistent node '%s' is missing a save() function, skipped" % node.name)
			continue
		var node_data = node.call("save")
		save_game.store_line(to_json(node_data))
	save_game.close()

func load_game(index):
	pass
