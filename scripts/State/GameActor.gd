extends Node
class_name GameActor

var location = "res://data/actors/Actor%03d.tres"

var index = 0
var data = null
var displayName = ""
var curr_level = 1
var curr_exp = 0
var curr_hp = 0
var curr_mp = 0
var paramUps = [0,0,0,0,0,0,0]
var skills = []

func setup(idx):
	index = idx
	data = load(location % idx)
	check_learnings()
	set_level(data.starting_level)
	curr_hp = mhp()
	curr_mp = mmp()
	displayName = data.defaultName

func faceGraphic():
	return data.defaultFace
func charGraphic():
	return data.defaultChar

func hp():
	return curr_hp
func mp():
	return curr_mp
func mhp():
	return data.get_health_param(0, curr_level) + paramUps[0]
func mmp():
	if (data.get_base_param(1) == 0):
		return 0 + paramUps[1]
	return data.get_health_param(1, curr_level) + paramUps[1]
func hpPerc():
	return hp() * 1.0 / mhp()
func mpPerc():
	return mp() * 1.0 / mmp()
func set_hp(v):
	v = clamp_scalar(v, mhp())
	curr_hp = v
func change_hp(v):
	set_hp(curr_hp + v)
func set_mp(v):
	v = clamp_scalar(v, mmp())
	curr_mp = v
func change_mp(v):
	set_mp(curr_mp + v)

func atk():
	return data.get_param(2, curr_level) + paramUps[2]
func def():
	return data.get_param(3, curr_level) + paramUps[3]
func mag():
	return data.get_param(4, curr_level) + paramUps[4]
func mdf():
	return data.get_param(5, curr_level) + paramUps[5]
func agi():
	return data.get_param(6, curr_level) + paramUps[6]

func level():
	return curr_level
func set_level(l):
	set_exp(data.next_lvl_exp(l-1))
func maximum_level():
	return data.maximum_level
func level_up():
	var pmhp = mhp()
	var pmmp = mmp()
	curr_level += 1
	change_hp(mhp()-pmhp)
	change_mp(mmp()-pmmp)
	check_learnings()
func level_down():
	curr_level -= 1

func check_learnings():
	for l in data.learnings:
		if (l[0] == curr_level):
			skills.append(l[1])

func change_exp(v):
	set_exp(curr_exp + v)
func set_exp(v):
	if (v < 0):
		v = 0
	curr_exp = v
	while (curr_level < maximum_level() && curr_exp >= next_lvl_exp()):
		level_up()
	while (curr_level > 1 && curr_exp < prev_lvl_exp()):
		level_down()

func total_exp(): # Total EXP earned
	return curr_exp
func total_exp_level(): # EXP to be acquired during current level
	return next_lvl_exp() - prev_lvl_exp()
func remaining_exp_next(): # EXP needed to level up
	return next_lvl_exp() - curr_exp
func prev_lvl_exp(): # Last EXP threshold to level up
	return data.next_lvl_exp(curr_level-1)
func curr_lvl_exp(): # Current EXP on this level
	return curr_exp - prev_lvl_exp()
func next_lvl_exp(): # EXP required to level up
	return data.next_lvl_exp(curr_level)
func expPerc():
	return curr_lvl_exp() * 1.0 / total_exp_level()

func clamp_scalar(v, maxVal):
	if (v < 0):
		return 0
	if (v > maxVal):
		return maxVal
	return v
