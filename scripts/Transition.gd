extends Control

onready var sprite = $TextureRect
onready var animation = $AnimationPlayer

func start_transition(name):
	visible = true
	var image = get_viewport().get_texture().get_data()
	image.flip_y()
	var texture = ImageTexture.new()
	texture.create_from_image(image)
	sprite.texture = texture
	animation.play(name)
	yield(animation, "animation_finished")
	visible = false
