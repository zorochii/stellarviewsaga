extends Node

export(NodePath) var statePath
export(NodePath) var interpreterPath

onready var state = get_node(statePath)
onready var interpreter = get_node(interpreterPath)

func _ready():
	pass
	
func _process(delta):
	if (state.transition.visible): return
	match(state.state):
		State.Scene.MAP:
			if (interpreter.busy()):
				return
			if (Input.is_action_just_pressed("ui_select")):
				open_menu()
		State.Scene.MENU:
			if (Input.is_action_just_pressed("ui_cancel")):
				close_menu()

func open_menu():
	state.transition.start_transition("Transition")
	state.state = State.Scene.MENU
	yield(get_tree(), "idle_frame")
	$PartyMenuManager.open_menu(state)
func close_menu():
	state.transition.start_transition("Transition")
	state.state = State.Scene.MAP
	yield(get_tree(), "idle_frame")
	$PartyMenuManager.close_menu()
