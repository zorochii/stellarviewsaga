tool
extends NinePatchRect
class_name SimpleSlider

export(NodePath) var barPath
export(float, 0, 1) var percent

var bar = null

func _ready():
	if (bar == null):
		bar = get_node(barPath)
	bar.anchor_right = percent

func _process(delta):
	if (bar == null):
		bar = get_node(barPath)
	bar.anchor_right = percent
