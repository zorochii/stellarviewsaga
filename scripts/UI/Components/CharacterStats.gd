extends Control

export(NodePath) var faceGraphicPath
export(NodePath) var nameLabelPath
export(NodePath) var levelValuePath
export(NodePath) var hpValuePath
export(NodePath) var mpValuePath
export(NodePath) var expValuePath
export(NodePath) var hpSliderPath
export(NodePath) var mpSliderPath
export(NodePath) var expSliderPath

onready var faceGraphic = get_node(faceGraphicPath)
onready var nameLabel = get_node(nameLabelPath)
onready var levelValue = get_node(levelValuePath)
onready var hpValue = get_node(hpValuePath)
onready var mpValue = get_node(mpValuePath)
onready var expValue = get_node(expValuePath)
onready var hpSlider = get_node(hpSliderPath)
onready var mpSlider = get_node(mpSliderPath)
onready var expSlider = get_node(expSliderPath)

var actor = null

func setup(a):
	actor = a
	faceGraphic.texture = actor.faceGraphic()
	nameLabel.text = actor.displayName
	refresh()

func refresh():
	hpValue.text = "%d" % actor.hp()
	mpValue.text = "%d" % actor.mp()
	levelValue.text = "Lv.%d" % actor.level()
	expValue.text = "%d/%d" % [actor.curr_lvl_exp(),actor.total_exp_level()]
	hpSlider.percent = actor.hpPerc()
	mpSlider.percent = actor.mpPerc()
	expSlider.percent = actor.expPerc()
