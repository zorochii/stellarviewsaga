extends Node

export(NodePath) var containerPath
export(NodePath) var itemPrototypePath

onready var container = get_node(containerPath)
onready var itemPrototype = get_node(itemPrototypePath)

func setup(party):
	for m in party.members:
		var n = itemPrototype.duplicate()
		container.add_child(n)
		var actor = party.get_actor(false, m)
		n.setup(actor)
		n.visible = true

func cleanup():
	var children = container.get_children()
	for c in children:
		if (c.visible):
			c.queue_free()
