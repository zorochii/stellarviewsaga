extends CanvasLayer

export(NodePath) var partyContainerPath

onready var partyContainer = get_node(partyContainerPath)

var state = null

func open_menu(s):
	state = s
	var party = state.get_node("Party")
	partyContainer.setup(party)
	visible = true

func close_menu():
	partyContainer.cleanup()
	visible = false
