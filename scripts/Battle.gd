extends Node2D

export(NodePath) var statePath
export(NodePath) var cameraPath

onready var state: State = get_node(statePath)
onready var camera: Camera2D = get_node(cameraPath)

func start_battle():
	state.transition.start_transition("BattleStart")
	state.state = State.Scene.BATTLE
	yield(get_tree(), "idle_frame")
	camera.current = true
	visible = true
	
func end_battle():
	state.transition.start_transition("BattleEnd")
	state.state = State.Scene.MAP
	yield(get_tree(), "idle_frame")
	var sceneCamera: Camera2D = state.get_node("Map").get_camera()
	visible = false
	sceneCamera.current = true

func _process(delta):
	if (visible):
		if (Input.is_action_just_pressed("ui_select")):
			end_battle()
