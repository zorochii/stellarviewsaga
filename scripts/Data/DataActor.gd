extends Resource
class_name DataActor

export(String) var defaultName
export(String) var description
export(Texture)  var defaultFace
export(Texture) var defaultChar
#                                        HP  MP ATK DEF MAG AGI
export(Array, int) var baseParameters = [100, 0, 10, 10, 10, 10]

export(int)  var starting_level = 1
export(int) var maximum_level = 99
export(int) var exp_base = 1
export(int) var exp_incr = 1
export(int) var exp_pow = 1

export(Array, Array, int) var learnings = [[1,1]]

func get_base_param(i):
	if (i < baseParameters.size()):
		return baseParameters[i]
	return 1

func get_health_param(id, level):
	var base = get_base_param(id)
	return 5 + (base / 4) + (base / 10 * level)

func get_param(id, level):
	var base = get_base_param(id)
	return (base / 8) + (base / 20 * level)
# (exp_base * l) + (sum_lvl * (1 + (exp_pow * sum_lvl / 1000))) * exp_incr
# (exp_base * l) + (sum_lvl * (1 + (sum_lvl * exp_pow / 1000))) * exp_incr
func next_lvl_exp(l):
	if (l < 1):
		return 0
	var sum_lvl = (l * (l-1)) / 2
	var power = 1 + (sum_lvl * exp_pow / 1000.0)
	return (exp_base * l) + (sum_lvl * power) * exp_incr
